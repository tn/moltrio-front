# Moltrio::Front

A small framework-agnostic library for decoupling models from forms in web
applications.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'moltrio-front'
```

And then execute:

    $ bundle

## Configuration

This section demonstrates a common configuration patterns, in a Rails
application.  However, it should work in any framework, provided that some
adjustments takes place.  A transaction implementation must be setup.  To do
so, reopen the `Moltrio::Front` module, and (for Rails) add the following:

```ruby
module Moltrio::Front
  def transaction
    ActiveRecord::Base.transaction do
      yield.tap do |result|
        raise ActiveRecord::Rollback unless result
      end
    end
  end
end
```

It's an instance method `#transaction` which takes a block, and should return
`true` in case of success, `false` otherwise (and in this case perform a
rollback before returning).  The block given to this method itself will return
`true` or `false`, whether it succeeds or not.  The `.transaction`
implementation from `ActiveRecord::Base` can be used to implement this
behavior.  Put the code above in `config/initializers/` to make sure it'll be
loaded at startup.  Please make the necessary adjustments if not using Rails
or ActiveRecord.

Another approach for configuration: create and additional module in the
application's `lib/` directory:

```ruby
module MyApp
  module Front
    def self.included(base)
      base.send(:include, Moltrio::Front)
    end

    def transaction
      ActiveRecord::Base.transaction do
        yield.tap do |result|
          raise ActiveRecord::Rollback unless result
        end
      end
    end
  end
end
```

Then, in each class which would otherwise include `Moltrio::Front`, do
`include MyApp::Front`.

Note: in any configuration pattern shown, the method still remains
overrideable per class.

## Usage

This section demonstrates a common usage pattern, in a Rails application.
Make the necessary adjustments if not using Rails or ActiveRecord.

Suppose that a person in an application can be entered in multiple contexts,
each context with specific validations, with specific different fields.  Using
the model for this, with its context-unaware validations, will make achieving
all these use-cases hard.  It would be a lot easier to do that if forms would
be build for intermediary objects, each object for a specific context.

Given a model class:

```ruby
class Person < ActiveRecord::Base
end
```

Instead of passing an instance of it directly to form helpers (`form_for` or
`simple_form_for`), you can wrap them using this library:

```ruby
class AgedPersonFront
  include Moltrio::Front
  include ActiveModel::Validations

  attribute :name
  attribute :age, Fixnum

  validates :age, presence: true

  persist def save
    model.save
  end
end
```

Then, in the controller:

```ruby
class AgedPeopleController < ApplicationController
  def new
    @person = AgedPersonFront.new(Person.new)
  end

  def create
    @person = AgedPersonFront.new(Person.new, params[:aged_person])

    if @person.save
      # ...
    else
      # ...
    end
  end
end
```

And in the view:

```erb
<%= form_for(@person) do |f| %>
  <%= f.label :name %>
  <%= f.text_field :name %>

  <%= f.label :age %>
  <%= f.text_field :age %>
<% end %>
```

### How it works

The front object defines two attributes: name and age.  They are just like
`Virtus` attributes, with more options.  They also accept coercions: in this
example, `age` is coerced to integers only.

The default initialization takes a model, and optionally instance attributes.

```ruby
class AgedPeopleController < ApplicationController
  def create
    @person = AgedPersonFront.new(Person.new, params[:aged_person])
    # ...
  end
end
```

Initially, name and age are copied from the model (a process that can be done
manually by calling `#load`), and then, if attributes are specified, set
according them.  Since the person is new, name and age are empty in the `new`
action, and filled-in with user-supplied values in the `create` action.

Eventually, the controller will call `#save`, defined as:

```ruby
class AgedPersonFront
  persist def save
    model.save
  end
end
```

When `#save` is called, thanks to the work done by the `persist` decorator,
all these things happen:

1. A transaction is started.  This is done by using the configured
   `#transaction` implementation.  A block is passed to it and everything runs
   inside it.
2. Validation runs through `#all_valid?`, which calls `#valid?`, which in
   turn is implemented by ActiveModel.
3. If the front is not valid, say, because an age was not present and it's
   required, then the process stops here, and the result of `#save` is
   `false`.
4. If the front is valid, model values are written to the `Person` model
   instance which was passed in initialization, with the parameters entered by
   the user, also passed in initialization.  This also can be done manually
   through `#flush`.
5. The front implementation is called.  In the example above, it just calls
   `#save` in the model, which will return `true` or `false`.
6. If it returned `false`, the process stops here, and the result of `#save`
   is `false`.
7. If it returned `true`, all model values are reloaded into the front
   instance, and the result of `#save` is `true`.

Then, as usual, the controller takes the result of `#save` and either shows
the same page to the user again with the validation errors or redirects.  Or
everything else: this is up to you.

The `#errors` method is also implemented by ActiveModel, and should work just
fine with Rails helpers.

### Loading and flushing values from and to the model

Front instances can work as a staging area for model changes.  They are
initialized with values provided by their model, and track changes to
themselves.  Values are restored from the model through `#load` and written to
the model through `#flush`.

Let's say that you have a `User` model, and a minimal `UserFront` front, both
with a `username` attribute:

```ruby
class User < ActiveRecord::Base; end

class UserFront
  include Moltrio::Front
  attribute :username

  persist def save
    model.save
  end
end

user = User.find_by_username('rcabralc')
user_front = UserFront.new(user)
user_front.username
#=> 'rcabralc'
```

Now, the front is changed:

```ruby
user_front.username = 'whatever'
user_front.username
#=> 'whatever'
user.username
#=> 'rcabralc'
user_front.changed?
#=> true
user_front.changed?(:username)
#=> true
```

If the front is `#load`ed, the username will be reset.  It'll be considered
not changed.

```ruby
user_front.load
user_front.username
#=> 'rcabralc'
user_front.changed?
#=> false
user_front.changed?(:username)
#=> false
```

Now lets change the model through the front:

```ruby
user_front.username = 'whatever'
user_front.changed?
#=> true
user_front.flush
user.username
#=> 'whatever'
```

Flushing values don't affect `#changed?`:

```ruby
user_front.changed?
#=> true
```

And now we save the model.  `#changed?` is reset to false.

```ruby
user_front.save
#=> true
user_front.changed?
#=> false
```

TODO: document how to customize loading and flushing.

### API

* `#model`
  Self-explanatory.
* `#load`
  Reads values from model and set them in the front.
* `#flush`
  Reads values from front and set them in the model.
* `#attributes`
  Serialize all attributes in a (possibly nested) Hash.
* `#attributes=(attributes)`
  Sets attributes given a (possibly nested) Hash of them.
* `#changed?(attribute = nil)`
  Self-explanatory.
* `#persisted?`
  Self-explanatory.
* `#valid?`
  Actually not an API method, but a convention.  Should be implemented by
  ActiveModel (or similar) or explicitly.  If no implementation is found, it
  is assumed to be `true`.
* `#all_valid?`
  Calls `#valid?` and also validates each nested front (TODO: document it).
* `#transaction(&block)`
  Actually not an API method, but a convention.  Should be implemented by the
  application.
* `.persist`
  A decorator which only calls the decorated method after successful
  validation, everything flushed to model, and inside a transaction.  The
  transaction is started before validation and flush.  If the decorated method
  returned true, it reloads model values and reset changes.
  TODO: this decorator is written in terms of other decorators, which can be
  used separately; document them.

### Expected API from models

* `#id` for persistence checks.  The method `#persisted?` will check for the
  presence of an id.
* Optional `#transaction(&block)` for transaction support.


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then,
run `rake false` to run the tests. You can also run `bin/console` for an
interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`, and then
run `bundle exec rake release`, which will create a git tag for the version,
push git commits and tags, and push the `.gem` file to
[rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/tn/moltrio-front.git.

