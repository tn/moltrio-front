require 'active_model'
require 'moltrio/front'

describe Moltrio::Front do
  let(:front_class) do
    Class.new do
      include Moltrio::Front

      attribute :answer
      attribute :guess
    end
  end

  let!(:front) { front_class.new(model) }
  let(:model) { Struct.new(:id, :answer, :guess).new(nil, 42, 0) }

  it "initializes the attribute according to the model" do
    expect(front.answer).to eq 42
  end

  it "allows changes in the attribute value" do
    front.answer = 666
    expect(front.answer).to eq 666
  end

  it "doesn't foward changes immediately to the model" do
    front.answer = 666
    expect(model.answer).to eq 42
  end

  it "forwards changes on #flush" do
    front.answer = 666
    front.flush

    expect(model.answer).to eq 666
  end

  it "doesn't read values from model after attribute initialization" do
    model.answer = 666
    expect(front.answer).to eq 42
  end

  it "can recover values from model on #load" do
    model.answer = 666
    front.load

    expect(front.answer).to eq 666
  end

  it "can set all attributes at once through #attributes=" do
    front.attributes = { answer: 666 }
    expect(front.answer).to eq 666
  end

  it "can get an attributes' hash from #attributes" do
    expect(front.attributes).to eq({ id: nil, answer: 42, guess: 0 })
  end

  it "can't set id through #attributes=" do
    front.attributes = { id: 666 }
    expect(front.id).to be_nil
  end

  specify "#id is not forwarded through #flush" do
    expect(model).to_not receive(:id=)
    front.flush
  end

  it "allows attribute reader override using `super'" do
    front_class.class_eval do
      def answer
        - super
      end
    end

    expect(front.answer).to eq(-42)
  end

  it "allows attribute writter override using `super'" do
    front_class.class_eval do
      def answer=(value)
        super(-value)
      end
    end

    front.answer = 42
    expect(front.answer).to eq(-42)
  end

  it "respects attribute overrides on #attributes" do
    front_class.class_eval do
      def answer
        - super
      end
    end

    expect(front.attributes).to eq({ id: nil, answer: -42, guess: 0 })
  end

  it "respects attribute overrides on #attributes=" do
    front_class.class_eval do
      def answer=(value)
        super(-value)
      end
    end

    front.attributes = { answer: 42 }
    expect(front.answer).to eq(-42)
  end

  describe "default values" do
    let(:front_class) do
      Class.new do
        include Moltrio::Front
        attribute :answer, default: 42
      end
    end

    let(:model) { Struct.new(:id, :answer).new }

    it "can be set through :default" do
      expect(front.answer).to eq 42
    end

    context "when something is set in the model" do
      let(:model) { Struct.new(:id, :answer).new(nil, 666) }

      specify "are ignored" do
        expect(front.answer).to eq 666
      end
    end

    context "using a symbol" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, default: :default_answer
          def default_answer; 42; end
        end
      end

      specify "make method named by the symbol to be called to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a symbol referencing another attribute" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, default: :default_answer
          attribute :default_answer, loader: 42
        end
      end

      specify "make method named by the symbol to be called to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a proc without arguments" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, default: Proc.new { default_answer }
          def default_answer; 42; end
        end
      end

      specify "instance-execs proc to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a lambda without arguments" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, default: -> { default_answer }
          def default_answer; 42; end
        end
      end

      specify "instance-execs lambda to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a proc" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, default: Proc.new { |front, model|
            model.default_answer
          }
        end
      end

      let(:model) do
        Struct.new(:id, :answer, :default_answer).new(nil, nil, 42)
      end

      specify "calls proc passing instance and model to get value" do
        expect(front.answer).to eq 42
      end
    end
  end

  describe "custom loader" do
    let(:front_class) do
      Class.new do
        include Moltrio::Front
        attribute :answer, loader: 42
      end
    end

    let(:model) { Struct.new(:id, :answer).new }

    it "can be set through :loader" do
      expect(front.answer).to eq 42
    end

    it "overrides the value from model" do
      model.answer = 666
      expect(front.answer).to eq 42
    end

    context "using a symbol" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, loader: :default_answer
          def default_answer; 42; end
        end
      end

      specify "make method named by the symbol to be called to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a symbol referencing another attribute" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, loader: :default_answer
          attribute :default_answer, loader: 42
        end
      end

      specify "make method named by the symbol to be called to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a proc without arguments" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, loader: Proc.new { default_answer }
          def default_answer; 42; end
        end
      end

      specify "instance-execs proc to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a lambda without arguments" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, loader: -> { default_answer }
          def default_answer; 42; end
        end
      end

      specify "instance-execs lambda to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using a proc" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, loader: Proc.new { |front, model|
            model.default_answer
          }
        end
      end

      let(:model) do
        Struct.new(:id, :answer, :default_answer).new(nil, nil, 42)
      end

      specify "calls proc passing instance and model to get value" do
        expect(front.answer).to eq 42
      end
    end

    context "using `false'" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, loader: false
        end
      end

      it "doesn't try to load value from model" do
        expect(model).to_not receive(:answer)
        front
      end

      it "lets the value unset" do
        expect(front.answer).to be nil
      end
    end
  end

  describe "custom flusher" do
    let(:model) { Struct.new(:id, :answer).new(nil, 42) }

    before do
      front.answer = 666
      front.flush
    end

    context "using a symbol" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, flusher: :flush_answer
          def flush_answer
            model.answer = -answer
          end
        end
      end

      specify "make method named by the symbol to be called to set value" do
        expect(model.answer).to eq(-666)
      end
    end

    context "using a proc without arguments" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, flusher: Proc.new { model.answer = -answer }
        end
      end

      specify "instance-execs proc to set value" do
        expect(model.answer).to eq(-666)
      end
    end

    context "using a lambda without arguments" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, flusher: -> { model.answer = -answer }
        end
      end

      specify "instance-execs lambda to set value" do
        expect(model.answer).to eq(-666)
      end
    end

    context "using a proc" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, flusher: Proc.new { |front, model, value|
            model.answer = -front.answer
          }
        end
      end

      specify "calls proc passing instance, model and value" do
        expect(model.answer).to eq(-666)
      end
    end

    context "using `false'" do
      let(:front_class) do
        Class.new do
          include Moltrio::Front
          attribute :answer, flusher: false
        end
      end

      it "keeps value set in front, without forwarding it to model" do
        expect(model.answer).to eq(42)
      end
    end
  end

  describe "changes tracking" do
    it "reports no change initially" do
      expect(front).to_not be_changed
    end

    specify "once something is set, the #changed? flag is flipped" do
      front.answer = 666
      expect(front).to be_changed
    end

    specify "a change can be reported for a single attribute" do
      front.guess = 42

      expect(front.changed?(:guess)).to be true
      expect(front.changed?(:answer)).to be false
    end

    specify "once everything is reverted, the #changed? flag is reset" do
      front.answer = 666
      front.answer = 42

      expect(front).to_not be_changed
    end

    specify "#load'ing values cause #changed? to be false" do
      front.answer = 666
      front.load

      expect(front).to_not be_changed
    end

    specify "#flush'ing changed values keep #changed?" do
      front.answer = 666
      front.flush

      expect(front).to be_changed
    end

    describe "persistence state" do
      context "when model id is nil" do
        it "reports unpersistence" do
          expect(front).to_not be_persisted
        end
      end

      context "when model has an id" do
        let(:model) { Struct.new(:id, :answer, :guess).new(0, 42, 0) }

        it "reports persistence" do
          expect(front).to be_persisted
        end
      end
    end

    describe ".reload decorator" do
      it "returns the result of the decorated method" do
        front_class.class_eval do
          reload def save
            flush
            model.id = 0
            'banana'
          end
        end

        expect(front.save).to eq 'banana'
      end

      context "after successful persistence attempt" do
        before do
          front_class.class_eval do
            reload def save
              flush
              model.id = 0
              true
            end
          end

          front.answer = 666
          front.save
        end

        it "resets changes" do
          expect(front).to_not be_changed
        end

        it "reports persistence if the model got an id" do
          expect(front).to be_persisted
        end
      end

      context "after failed persistence attempt" do
        before do
          front_class.class_eval do
            reload def save
              flush
              model.id = 0
              false
            end
          end

          front.answer = 666
          front.save
        end

        it "doesn't reset changes" do
          expect(front).to be_changed
        end

        it "doesn't report persistence even if the model got an id" do
          expect(front).to_not be_persisted
        end
      end
    end
  end

  describe ".transactional decorator" do
    context "with proper transaction implementation" do
      let(:rollback_exception) { Class.new(StandardError) }

      let(:transaction_manager) do
        exc = rollback_exception

        Object.new.tap do |object|
          object.instance_eval { @rollback = false }

          object.define_singleton_method(:rollback!) do
            @rollback = true
          end

          object.define_singleton_method(:rolled_back?) { @rollback }

          object.define_singleton_method(:transaction) do |&block|
            begin
              block.call
            rescue exc
              rollback!
              false
            rescue
              rollback!
              raise
            end
          end
        end
      end

      before do
        manager = transaction_manager
        exc = rollback_exception

        front_class.class_eval do
          define_method(:transaction) do |&block|
            manager.transaction do
              block.call.tap { |result| fail exc unless result }
            end
          end

          transactional def save
            model.save
          end
        end
      end

      it "bubbles non-rollback exceptions" do
        exc = Class.new(StandardError)
        model.define_singleton_method(:save) { fail exc }
        expect(-> { front.save }).to(raise_error(exc))
      end

      it "swallows rollback exception" do
        exc = rollback_exception
        model.define_singleton_method(:save) { fail exc }
        expect(-> { front.save }).to_not(raise_error)
      end

      it "returns false on rollback exception" do
        exc = rollback_exception
        model.define_singleton_method(:save) { fail exc }
        expect(front.save).to be false
      end

      it "rolls back transaction if model can't be saved" do
        model.define_singleton_method(:save) { false }
        front.save
        expect(transaction_manager.rolled_back?).to be true
      end

      it "returns true if model can be saved" do
        model.define_singleton_method(:save) { true }
        expect(front.save).to be true
      end

      it "doesn't rollback if model can be saved" do
        model.define_singleton_method(:save) { true }
        expect(transaction_manager.rolled_back?).to be false
      end
    end

    context "with custom transactions" do
      before do
        front_class.class_eval do
          def transaction
            model.custom_transaction { yield }
          end

          transactional def save
            model.custom_transaction?
          end
        end

        model.class.class_eval do
          def custom_transaction(&block)
            @custom_transaction = true
            block.call
          ensure
            @custom_transaction = false
          end

          def custom_transaction?
            @custom_transaction
          end
        end
      end

      it "opens the transaction according to the customization" do
        expect(front.save).to be true
      end
    end
  end

  describe "validations" do
    context "using ActiveModel" do
      before do
        model.define_singleton_method(:save) { }

        front_class.class_eval do
          include ActiveModel::Validations
          validates :answer, presence: true

          # Unamed classes won't work with ActiveModel.
          def self.name
            "Question"
          end

          valid def save
            model.save
          end
        end
      end

      it "can be validated" do
        front.answer = nil
        expect(front).to_not be_valid

        front.answer = 666
        expect(front).to be_valid
      end

      describe ".valid decorator" do
        it "doesn't call method if invalid" do
          front.answer = nil
          expect(model).to_not receive(:save)
          front.save
        end

        it "calls method if valid" do
          front.answer = 666
          expect(model).to receive(:save).once
          front.save
        end
      end
    end
  end

  context "without a model" do
    let(:model) { nil }

    it "gets and sets its attributes" do
      front.answer = 42
      expect(front.answer).to eq 42
    end
  end

  context "with nested front" do
    let(:front_class) do
      condition = self.condition

      Class.new do
        include Moltrio::Front

        attribute :answer
        nested :nested, if: -> { condition.value } do
          attribute :answer

          def expose_parent
            parent_front
          end
        end
      end
    end

    let(:condition) { Struct.new(:value).new(true) }
    let(:nested) { Struct.new(:id, :answer).new(nil, 42) }
    let(:model) { Struct.new(:id, :answer, :nested).new(nil, 42, nested) }

    it "can get values from nested front" do
      expect(front.answer).to eq 42
      expect(front.nested.answer).to eq 42
    end

    it "can set values to nested front" do
      front.answer = 666
      front.nested.answer = 666

      expect(front.answer).to eq 666
      expect(front.nested.answer).to eq 666
    end

    it "cannot set the nested front directly" do
      expect { front.nested = 'something' }.to raise_error(NoMethodError)
    end

    it "makes nested front know about its parent" do
      expect(front.nested.expose_parent).to be front
    end

    specify "value set in nested front is not set into the nested model" do
      front.nested.answer = 666
      expect(nested.answer).to eq 42
    end

    describe "#flush" do
      before do
        front.answer = 666
        front.nested.answer = 666
      end

      it "works in cascade" do
        front.flush

        expect(model.answer).to eq 666
        expect(nested.answer).to eq 666
      end

      it "doesn't attempt to set #nested= in model if no flusher defined" do
        expect(model).to_not receive(:nested=)
        front.flush
      end

      it "is skipped if not allowed through a condition" do
        condition.value = false
        front.flush

        expect(model.answer).to eq 666
        expect(nested.answer).to eq 42
      end
    end

    describe "#load" do
      before do
        model.answer = 666
        nested.answer = 666
      end

      it "works in cascade" do
        front.load

        expect(front.answer).to eq 666
        expect(front.nested.answer).to eq 666
      end
    end

    describe "#attributes" do
      it "works in cascade" do
        front.answer = 666
        front.nested.answer = 666

        expect(front.attributes).to eq({
          id: nil,
          answer: 666,
          nested: {
            id: nil,
            answer: 666
          }
        })
      end
    end

    describe "#attributes=" do
      it "works in cascade" do
        front.attributes = { answer: 666, nested: { answer: 666 } }

        expect(front.answer).to eq 666
        expect(front.nested.answer).to eq 666
      end

      it "is skipped if not allowed through a condition" do
        condition.value = false
        front.attributes = { answer: 666, nested: { answer: 666 } }

        expect(front.answer).to eq 666
        expect(front.nested.answer).to eq 42
      end
    end

    describe ".valid decorator" do
      before do
        model.define_singleton_method(:save) { }

        front_class.class_eval do
          valid def save
            model.save
          end
        end
      end

      it "works in cascade when invalid" do
        front.define_singleton_method(:valid?) { true }
        front.nested.define_singleton_method(:valid?) { false }

        expect(model).to_not receive(:save)
        front.save
      end

      it "works in cascade when valid" do
        front.define_singleton_method(:valid?) { true }
        front.nested.define_singleton_method(:valid?) { true }

        expect(model).to receive(:save).once
        front.save
      end
    end
  end

  describe ".to_proc" do
    it "returns a proc for the .new method" do
      instance = front_class.send(:to_proc).call(model)
      expect(instance).to be_a(front_class)
      expect(instance.model).to eq model
    end

    it "marks the proc as 'evaluating to front'" do
      proc = front_class.send(:to_proc)
      expect(proc).to evaluate_to_front
    end

    def evaluate_to_front
      be_evaluates_to_front
    end
  end
end
