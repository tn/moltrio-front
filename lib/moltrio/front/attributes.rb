require 'moltrio/decorator'

module Moltrio
  module Front
    class UnboundAttribute
      attr_accessor :condition

      def initialize(name, type, **options, &block)
        @name = name
        @type = type
        @options = options
        @block = block
        @condition = -> { true }
      end

      def bind(front, model)
        if nested?
          NestedAttribute.new(front, model, name, condition, **options, &block)
        else
          Attribute.new(front, model, name, type, **options)
        end
      end

      def readonly?
        options[:readonly] || nested?
      end

    private

      def nested?
        !!block
      end

      def name; @name; end
      def type; @type; end
      def options; @options; end
      def block; @block; end
    end

    class UnboundCollectionAttribute
      def initialize(name, **options, &block)
        @name = name
        @options = options
        @block = block
      end

      def bind(front, model)
        NestedCollectionAttribute.new(front, model, name, **options, &block)
      end

    private

      def name; @name; end
      def options; @options; end
      def block; @block; end
    end

    module AttributeDecorators
      extend Moltrio::Decorator

      decorator def once(method)
        -> *a, &b do
          define_singleton_method(method) { }
          super(*a, &b)
        end
      end

      decorator def load(method)
        -> *a, &b { load; super(*a, &b) }
      end
    end

    class BaseAttribute
      extend AttributeDecorators

      OPTIONS = %i(map loader flusher default readonly)

      def initialize(front, model, name, **options)
        if options.key?(:default) && options.key?(:loader)
          fail ArgumentError, "use :default or :loader, but not both"
        end

        @front = front
        @model = model
        @name = name
        @options = options
        @map = options.fetch(:map) { name }
      end

      def nested?
        false
      end

      def valid?
        true
      end

      def attributes=(*)
      end

      def errors
      end

    private

      def front; @front; end
      def model; @model; end
      def name; @name; end
      def map; @map; end
      def options; @options; end

      def default
        @default ||= build_default(options.fetch(:default) { -> _, _ { } })
      end

      def loader
        @loader ||= build_loader(options.fetch(:loader) {
          -> _, _ {
            value = model.public_send(map) if model && map
            value.nil?? default.call : value
          }
        })
      end

      def flusher
        @flusher ||= build_flusher(options.fetch(:flusher) {
          -> *, value { model.public_send(:"#{map}=", value) if map }
        })
      end

      def build_default(default)
        if default.is_a?(Proc)
          if default.arity == 0
            -> { front.instance_exec(&default) }
          else
            -> { default.call(front, model) }
          end
        elsif default.is_a?(Symbol)
          -> { front.send(default) }
        else
          -> { default }
        end
      end

      def build_loader(loader)
        return -> { } unless loader

        if loader.is_a?(Proc)
          if loader.arity == 0
            -> { front.instance_exec(&loader) }
          else
            -> { loader.call(front, model) }
          end
        elsif loader.is_a?(Symbol)
          -> { front.send(loader) }
        else
          -> { loader }
        end
      end

      def build_flusher(flusher)
        return -> _ { } unless flusher

        if flusher.is_a?(Proc)
          if flusher.arity == 0
            -> _ { front.instance_exec(&flusher) if model }
          else
            -> value { flusher.call(front, model, value) if model }
          end
        elsif flusher.is_a?(Symbol)
          -> value { front.send(flusher) if model }
        else
          fail "Invalid flusher: #{flusher}"
        end
      end

      def readonly?
        options[:readonly]
      end
    end

    class Attribute < BaseAttribute
      def initialize(front, model, name, type, **options)
        super(front, model, name, **options)
        @type = type
        @attr_options = { lazy: true, nullify_blank: true }.merge(
          options.reject { |k| BaseAttribute::OPTIONS.include?(k) }
        )
        @object = nil
      end

      def value
        object.value
      end

      def value=(value)
        object.value = value unless readonly?
      end

      def attributes
        value
      end

      once def load
        @object = Object.new.tap do |object|
          object.extend(Virtus.model)
          object.attribute(:value, type, attr_options)
          @original_value = object.value = loader.call
        end
      end

      def flush
        flusher.call(value)
      end

      def changed?
        value != original_value
      end

      load def original_value; @original_value; end

    private

      load def object; @object; end
      def type; @type; end
      def attr_options; @attr_options; end
    end

    class NestedAttribute < BaseAttribute
      def initialize(front, model, name, condition = nil, **options, &block)
        super(front, model, name, **options)
        @block = block
        @object_factory = nil
        @condition = build_condition(condition)
      end

      once def load
        return unless object_factory
        @object = object_factory.call(loader.call)
      end

      def flush
        return if skip?
        object.flush
        flusher.call(object.model)
      end

      def changed?
        object.changed?
      end

      def value
        object
      end

      def attributes
        object.attributes
      end

      def attributes=(attributes)
        return if skip?
        object.attributes = attributes || {}
      end

      def valid?
        return true if skip?
        object.all_valid?
      end

      def errors
        object.errors
      end

      def nested?
        true
      end

    private

      def block; @block; end
      load def object; @object; end

      def flusher
        if options.key?(:flusher)
          super
        else
          @flusher ||= -> _ { }
        end
      end

      def object_factory
        return @object_factory if @object_factory
        return unless block

        if block.respond_to?(:evaluates_to_front?) && block.evaluates_to_front?
          factory = block
        else
          factory = Class.new.tap do |o|
            o.class_exec(block) do |block|
              include Moltrio::Front
              class_eval(&block)
            end
          end.send(:to_proc)
        end

        @object_factory = -> *a, &b { factory.call(*a, &b).tap do |instance|
          instance.instance_variable_set(:@parent_front, front)
        end }
      end

      def build_condition(condition)
        return -> { true } unless condition

        if condition.is_a?(Proc)
          if condition.arity == 0
            -> { front.instance_exec(&condition) }
          else
            -> { condition.call(front) }
          end
        elsif condition.is_a?(Symbol)
          -> { front.send(condition) }
        else
          fail "invalid condition type: #{condition.inspect}"
        end
      end

      def skip?
        !@condition.call
      end
    end

    class NestedCollectionAttribute < NestedAttribute
      def initialize(front, model, name, **kw, &block)
        super(front, model, name, **kw)
        @builder = build_builder(kw[:builder])
      end

      once def load
        return [] unless object_factory
        @objects = loader.call.map(&object_factory)
      end

      def flush
        objects.each(&:flush)
        flusher.call(objects.map(&:model))
      end

      def changed?
        objects.any?(&:changed?)
      end

      def value
        objects.dup
      end

      def attributes
        objects.map(&:attributes)
      end

      def attributes=(attributes_collection)
        to_update_attributes, to_remove_attributes, to_create_attributes =
          split_atttributes_collection(attributes_collection)

        to_update_ids = Set[*to_update_attributes.map { |a| a['id'].to_s }]
        existing_ids = Set[*objects.map(&:id).map(&:to_s)]
        unless existing_ids.superset?(to_update_ids)
          fail "Not found ids: #{to_update_ids - existing_ids}."
        end

        update_fronts_from_attributes(to_update_attributes)
        remove_fronts_from_attributes(to_remove_attributes)
        create_fronts_from_attributes(to_create_attributes)
      end

      def valid?
        objects.all?(&:all_valid?)
      end

      def errors
        objects.map(&:errors)
      end

      def add(attributes)
        objects.push(build(attributes))
      end

    private

      def builder; @builder; end
      load def objects; @objects; end

      def build_builder(builder)
        fail "Builder not set for #{front.class}##{name}" if !builder

        if builder.is_a?(Proc)
          if builder.arity == 0
            -> { front.instance_exec(&builder) }
          else
            -> { builder.call(front) }
          end
        elsif builder.is_a?(Symbol)
          -> { front.send(builder) }
        else
          fail "Invalid builder: #{builder}"
        end
      end

      def split_atttributes_collection(collection)
        collection = normalize_atttributes_collection(collection)

        to_update = collection.select do |a|
           present?(a['id']) && !to_boolean(a['_destroy'])
        end

        to_remove = collection.select do |a|
           present?(a['id']) &&  to_boolean(a['_destroy'])
        end

        to_create = collection.select do |a|
          !present?(a['id']) && !to_boolean(a['_destroy'])
        end

        [to_update, to_remove, to_create]
      end

      def normalize_atttributes_collection(collection)
        return collection.values if collection.is_a?(Hash)
        (collection || []).map(&method(:stringify_keys))
      end

      def update_fronts_from_attributes(to_update_attributes)
        to_update_attributes.each do |attributes|
          objects
            .detect { |f| f.id.to_s == attributes['id'].to_s }
            .attributes = attributes
        end
      end

      def remove_fronts_from_attributes(to_remove_attributes)
        to_remove_ids = Set[*to_remove_attributes.map { |a| a['id'].to_s }]
        objects.delete_if do |front|
          to_remove_ids.include?(front.id.to_s)
        end
      end

      def create_fronts_from_attributes(to_create_attributes)
        to_create_attributes.each(&method(:add))
      end

      def build(attributes = nil)
        object_factory.call(builder.call, attributes)
      end

      def stringify_keys(hash)
        Hash[*hash.map { |k, v| [k.to_s, v] }.flatten(1)]
      end

      def present?(value)
        value && value != ''
      end

      def to_boolean(value)
        ![nil, false, '0', '', 'false'].include?(value)
      end
    end
  end
end
