require 'set'
require 'virtus'
require "moltrio/front/version"
require "moltrio/front/attributes"
require 'moltrio/decorator'

module Moltrio
  module Front
    def self.included(base)
      base.extend(Macros)
      base.extend(Decorators)
      base.attribute(:id, readonly: true, flusher: false)
    end

    module Macros
      def model_method(name = nil)
        if name
          @model_method = name
        else
          @model_method || :model
        end
      end

      def attribute(name, type = nil, **options, &block)
        fail TypeError, "not a symbol: #{name.inspect}" if !name.is_a?(Symbol)

        mod = attribute_accessors_module

        attributes[name] = UnboundAttribute.new(name, type, **options, &block)
        mod.send(:define_method, name) { attribute_objects[name].value }

        unless attributes[name].readonly?
          mod.send(:define_method, :"#{name}=") do |v|
            attribute_objects[name].value = v
          end
        end

        attributes[name]
      end

      def nested(name, type = nil, **options, &block)
        fail ArgumentError, "a block is required" unless block
        condition = options.delete(:if)
        attribute(name, type, **options, &block).tap do |attr|
          attr.condition = condition
        end
      end

      def collection(name, **options, &block)
        fail TypeError, "not a symbol: #{name.inspect}" if !name.is_a?(Symbol)
        fail ArgumentError, "a block is required" unless block

        mod = attribute_accessors_module

        attributes[name] = UnboundCollectionAttribute
          .new(name, **options, &block)
        mod.send(:define_method, name) { attribute_objects[name].value }
      end

      def attributes
        @attributes ||= {}.merge(
          superclass && superclass.is_a?(Macros) ? superclass.attributes : {}
        )
      end

    private

      def to_proc
        # Unamed class: keep reference to `self', since there's no way to
        # access the class later.
        return make_proc(method(:new)) unless name

        # Named class: avoid a reference to `self' to allow this class to be
        # unloaded and replaced, by enclosing needed stuff in a struct and
        # using a method from it as the returned proc.  The name is the
        # reference for any newer version of this class.
        make_proc(Struct.new(:name).new(name).tap do |context|
          context.define_singleton_method(:m) do |*a, &b|
            path = name.split('::')
            path.inject(Object.const_get(path.shift), &:const_get).new(*a, &b)
          end
        end.method(:m))
      end

      def make_proc(method_object)
        method_object.to_proc.tap do |proc|
          proc.define_singleton_method(:evaluates_to_front?) { true }
        end
      end

      def attribute_accessors_module
        @attribute_accessors_module ||= Module.new.tap do |mod|
          include(mod)
        end
      end
    end
    private_constant :Macros

    module Decorators
      extend Moltrio::Decorator

      decorator def valid(method)
        -> *args, &block do
          return false unless all_valid?
          super(*args, &block)
        end
      end

      decorator def reload(method)
        -> *args, &block do
          super(*args, &block).tap { |result| load if result }
        end
      end

      decorator def flushed(method)
        -> *args, &block { flush; super(*args, &block) }
      end

      decorator def transactional(method)
        -> *args, &block do
          transaction { super(*args, &block) }
        end
      end

      def persist(method)
        persist_unflushed(flushed(method))
      end

      def persist_unflushed(method)
        reload(transactional(valid(method)))
      end
    end

    extend Decorators

    def initialize(model, attributes = {})
      @model = model
      @attribute_objects = nil
      self.attributes = attributes
    end

    def persisted?
      !!id
    end

    def load
      @attribute_objects = nil
      attribute_objects
      model
    end

    def flush
      attribute_objects.values.each(&:flush)
      model
    end

    def attributes
      Hash[*attribute_objects.map do |key, attribute|
        if attribute.nested?
          value = attribute.attributes
        else
          value = send(key)
        end

        [key, value]
      end.flatten(1)]
    end

    def attributes=(attributes)
      attributes ||= {}

      flat_allowed_keys = attribute_objects
        .reject { |k, v| v.nested? }.keys.map(&:to_s)
      nested_allowed_keys = attribute_objects
        .select { |k, v| v.nested? }.keys.map(&:to_s)

      flat_keys = attributes.keys.select do |key|
        flat_allowed_keys.include?(key.to_s)
      end

      nested_keys = attributes.keys.select do |key|
        nested_allowed_keys.include?(key.to_s)
      end

      flat_keys.each do |key|
        setter = :"#{key}="
        send(setter, attributes[key]) if respond_to?(setter, true)
      end

      nested_keys.each do |key|
        attribute = attribute_objects[key.to_sym]
        attribute.attributes = attributes[key]
      end
    end

    def changed?(attribute = nil)
      if attribute
        unless attribute_objects.key?(attribute)
          fail "unknown attribute: #{attribute}"
        end

        attribute_objects[attribute].changed?
      else
        attribute_objects.values.any?(&:changed?)
      end
    end

    def model
      return @model if defined?(@model)
      @model = send(self.class.model_method)
    end

    def parent_front
      @parent_front
    end

    def all_valid?
      self_result = valid?
      cascade_result = attribute_objects.values.map(&:valid?)

      [self_result, *cascade_result].all?.tap do |result|
        nested_fronts.each do |key, nested_front|
          next unless respond_to?(:assign_nested_errors)

          if nested_front.respond_to?(:each)
            nested_front.each.with_index do |front, i|
              assign_nested_errors(key, front, i)
            end
          else
            assign_nested_errors(key, nested_front)
          end
        end unless result
      end
    end

  private

    def nested_fronts
      Hash[*
        attribute_objects
          .select { |name, attribute| attribute.value.is_a?(Front) }
          .map { |name, attribute| [name, attribute.value] }
          .flatten(1)
      ]
    end

    def transaction
      yield
    end

    def attribute_objects
      return @attribute_objects if @attribute_objects
      (@attribute_objects = build_attributes).tap do |hash|
        hash.values.each(&:load)
      end
    end

    def build_attributes
      Hash[*self.class.attributes.map do |name, unbound|
        [name, unbound.bind(self, model)]
      end.flatten(1)]
    end
  end
end
