# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'moltrio/front/version'

Gem::Specification.new do |spec|
  spec.name          = "moltrio-front"
  spec.version       = Moltrio::Front::VERSION
  spec.authors       = ["rcabralc"]
  spec.email         = ["rcabralc@gmail.com"]

  spec.summary       = %q{A small library for decoupling models from web forms.}
  spec.description   = %q{A small framework-agnostic library for decoupling models from forms in web applications.}
  spec.homepage      = "https://gitlab.com/tn/moltrio-front.git"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "http://mygemserver.com"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "moltrio-decorator", "~> 0.1.0"
  spec.add_dependency "virtus"

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.3.0"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "pry-doc"
  spec.add_development_dependency "pry-stack_explorer"
  spec.add_development_dependency "pry-byebug"
  spec.add_development_dependency "pry-theme"
  spec.add_development_dependency "activemodel"
end
